export interface ProductsModel {
  id: number;
  name: string;
  price: number;
  sale?: boolean;
  saleAmount?: number;
  monthlyPayment: number;
  image?: string;
}
