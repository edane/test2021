import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ProductsListComponent } from './products-list/products-list.component';
import { BannerComponent } from './products-list/banner/banner.component';
import { ItemComponent } from './products-list/item/item.component';
import {CommonModule} from "@angular/common";
import { CheckoutComponent } from './products-list/checkout/checkout.component';

@NgModule({
  declarations: [
    AppComponent,
    ProductsListComponent,
    BannerComponent,
    ItemComponent,
    CheckoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    CommonModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
