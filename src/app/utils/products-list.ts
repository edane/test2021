import {ProductsModel} from "../models/products.model";

export const list: ProductsModel[] = [
  {
    id: 1,
    name: 'Laptop',
    price: 300,
    sale: false,
    monthlyPayment: 29,
    image: 'laptop'
  },
  {
    id: 1,
    name: 'Air Conditioner',
    price: 400,
    sale: false,
    monthlyPayment: 29,
    image: 'aircon'
  },
  {
    id: 1,
    name: 'TV',
    price: 700,
    sale: false,
    monthlyPayment: 29,
    image: 'tv'
  }
]
