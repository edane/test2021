import {Injectable, OnDestroy} from "@angular/core";
import {Observable, of, Subject} from "rxjs";
import {ProductsModel} from "../models/products.model";
import {share} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class StorageService implements OnDestroy {
  private storageSubject = new Subject<ProductsModel | any>();
  public changes = this.storageSubject.asObservable().pipe(share());

  constructor() {
    this.start();
  }

  start() {
    window.addEventListener("storage", this.pushEvent.bind(this));
  }

  public getItemsByKey(key: string): Observable<any> {
    return of(JSON.parse(localStorage.getItem(key) || "[]"));
  }

  public store(key: string, product: ProductsModel | any): void {
    let storedProducts: ProductsModel[] = this.parseStorageItems(key) ?? [];
    storedProducts.push(product)
    localStorage.setItem(key, JSON.stringify(storedProducts));
    this.storageSubject.next((product))
  }

  public remove(key: string, id: number, index: number) {
    let storedProducts: ProductsModel[] = this.parseStorageItems(key) ?? [];
    storedProducts[index]?.id === id ? storedProducts?.splice(index, 1) : null;
    localStorage.setItem(key, JSON.stringify(storedProducts));
  }


  stop() {
    window.removeEventListener("storage", this.pushEvent.bind(this));
    this.storageSubject.complete();
  }

  pushEvent(event: StorageEvent) {
    if (event.newValue) {
      this.storageSubject.next(event.newValue);
    }
  }

  private parseStorageItems(key: string) {
    return JSON.parse(<string>localStorage.getItem(key || "[]"));
  }

  ngOnDestroy() {
    this.stop();
  }

}
