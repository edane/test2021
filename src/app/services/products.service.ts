import {Injectable} from "@angular/core";
import {ProductsModel} from "../models/products.model";
import {StorageService} from "./storage.service";
import {Observable, of} from "rxjs";
import {map} from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})

export class ProductsService {

  constructor(private storageService: StorageService) {
  }

  public getProductsList(): Observable<ProductsModel[]> {
    return this.storageService.getItemsByKey('products');
  }

  public addNewProduct(key: string, product: ProductsModel) {
    this.storageService.store(key, product)
  }

  public  deleteProduct(id: number, index: number) {
    this.storageService.remove('products', id, index);
  }
}
