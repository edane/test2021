import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemComponent } from './item.component';
import {oneOf} from "../../utils/oneOf";

describe('ItemComponent', () => {
  let component: ItemComponent;
  let fixture: ComponentFixture<ItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it("should add new on click", () => {
    spyOn(component.onAddProduct, "emit");
    component.onAddNew();
    expect(component.onAddProduct.emit).toHaveBeenCalled();
  });

  it("should delete product on click", () => {
    spyOn(component.onDeleteProduct, "emit");
    component.deleteProduct(oneOf([1, 2, 3]));
    expect(component.onDeleteProduct.emit).toHaveBeenCalled();
  });
});
