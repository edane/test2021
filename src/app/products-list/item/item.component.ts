import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {ProductsModel} from "../../models/products.model";

@Component({
  selector: 'app-item',
  templateUrl: './item.component.html',
  styleUrls: ['./item.component.scss']
})
export class ItemComponent implements OnInit {
  @Input() item!: ProductsModel;
  @Input() last!: boolean;
  @Output() onDeleteProduct: EventEmitter<any> = new EventEmitter<any>();
  @Output() onAddProduct: EventEmitter<any> = new EventEmitter<any>();

  constructor() { }

  ngOnInit(): void {
  }

  deleteProduct(id: number | undefined) {
    this.onDeleteProduct.emit(id);
  }

  onAddNew() {
    this.onAddProduct.emit();
  }

}
