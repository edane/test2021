import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductsListComponent } from './products-list.component';
import {ProductsService} from "../services/products.service";
import {StorageService} from "../services/storage.service";
import {ProductsModel} from "../models/products.model";
import {list} from "../utils/products-list";
import {oneOf} from "../utils/oneOf";
import {ItemComponent} from "./item/item.component";
import {BannerComponent} from "./banner/banner.component";
import {CheckoutComponent} from "./checkout/checkout.component";

describe('ProductsListComponent', () => {
  let component: ProductsListComponent;
  let fixture: ComponentFixture<ProductsListComponent>;
  let availableProducts: ProductsModel[] = list;
  let productsService: ProductsService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProductsListComponent, ItemComponent, BannerComponent, CheckoutComponent ],
      providers: [
        ProductsService,
        StorageService
      ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductsListComponent);
    component = fixture.componentInstance;
    productsService = TestBed.inject(ProductsService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should get products', () => {
    spyOn(productsService, "getProductsList").and.callThrough();
    component.getProducts();
    expect(productsService.getProductsList).toHaveBeenCalled();
  });

  it('should add new product', () => {
    spyOn(productsService, "addNewProduct").and.callThrough();
    component.addNew();
    expect(productsService.addNewProduct).toHaveBeenCalled();
  });

  it('should delete product', () => {
    spyOn(productsService, "deleteProduct").and.callThrough();
    spyOn(component, "getProducts").and.callThrough();
    component.deleteProduct(oneOf([1, 2, 3]), oneOf(availableProducts));
    expect(productsService.deleteProduct).toHaveBeenCalled();
    expect(component.getProducts).toHaveBeenCalled();
  });

  it('should get products count', () => {
    spyOn(component, "getProductsCount").and.callThrough();
    component.productsList = list;
    component.getProductsCount();
    expect(component.getProductsCount()).toBeGreaterThan(0)
  });

  it('should calculate products value per month', () => {
    spyOn(component, "getProductsValue").and.callThrough();
    component.productsList = list;
    component.getProductsValue();
    expect(component.getProductsValue()).toBeGreaterThan(0)
  });

});
