import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  @Input() itemCount!: number;
  @Input() totalPerMonth!: number;

  constructor() { }

  ngOnInit(): void {
  }

}
