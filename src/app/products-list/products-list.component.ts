import {Component, OnInit} from '@angular/core';
import {ProductsService} from "../services/products.service";
import {list} from "../utils/products-list"
import {ProductsModel} from "../models/products.model";
import {oneOf} from "../utils/oneOf";
import {StorageService} from "../services/storage.service";

@Component({
  selector: 'app-products-list',
  templateUrl: './products-list.component.html',
  styleUrls: ['./products-list.component.scss']
})
export class ProductsListComponent implements OnInit {
  availableProducts: ProductsModel[] = list;
  productsList: ProductsModel[] = [];
  leasingPeriod = 12;

  constructor(private productsService: ProductsService,
              private storage: StorageService) {

  }

  ngOnInit(): void {
    this.getProducts();
    this.storage.changes.subscribe(change => {
      this.productsList.push(change)
    })
  }

  getProducts() {
    this.productsService.getProductsList()
      .subscribe(products => this.productsList = products);
  }

  addNew() {
    const productToAdd: ProductsModel = oneOf(this.availableProducts);
    this.productsService.addNewProduct('products', productToAdd);
  }

  deleteProduct(event: number, index: number) {
    this.productsService.deleteProduct(event, index);
    this.getProducts();
  }

  getProductsCount(): number {
    return this.productsList.length;
  }

  getProductsValue(): number {
    let sum = 0;
    this.productsList.forEach( product => {
      sum += product.monthlyPayment;
    })
    return sum;
  }

}
